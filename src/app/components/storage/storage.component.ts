import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-storage',
  templateUrl: './storage.component.html',
  styleUrls: ['./storage.component.css']
})
export class StorageComponent implements OnInit {

  nombre!: string;
  nombre2!: string;

  constructor() { }

  ngOnInit(): void {
    this.nombre = 'Mercedes Sosa';
    this.nombre2 = 'Pedro Meneces';

    //GUARDAR datos
    //setItem('clave', valor)
    sessionStorage.setItem('nombre', this.nombre);

    //RECUPERAR datos
    //getItem('clave', valor)
    // let resultado = sessionStorage.getItem('nombre');
    // console.log(resultado);

    //REMOVER un item del sessionStorage
    //removeItem('clave')
    // sessionStorage.removeItem('nombre');
    // resultado = sessionStorage.getItem('nombre');
    // console.log(resultado);
    
    //LIMPIAR todo el item
    // sessionStorage.clear();
    // resultado = sessionStorage.getItem('nombre');
    // console.log(resultado);
    
    //GUARDAR datos
    localStorage.setItem ('nombre2', this.nombre2);
    //RECUPERAR datos
    // let resultado = localStorage.getItem('nombre2');
    // console.log(resultado);
    //REMOVER item
    // localStorage.removeItem('nombre2');
    // resultado = localStorage.getItem('nombre2');
    // console.log(resultado);
    //LIMPIAR
    // localStorage.clear();
    // let resultado = localStorage.getItem('nombre2');
    // console.log(resultado);     
    
  }

}
